﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <Windows.h>
#include "locality.h"
#include "coordinates.h"
using namespace std;

int main() {
	const int length = 3;

	/*Вывод на русском языке*/
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	/*----------------------*/

	// Конструктор без параметров
	Locality localityWithEmptyConstructor;

	// Конструктор с параметрами (2 параметра)
	Locality localityWithTwoParamsConstructor = Locality("Penza", "Russia");
	
	// Конструктор с параметрами (6 параметров)
	Locality localityWithFullConstructor = Locality("Penza", 520300, Coordinates(53.12, 45.0), 1663, "Russia", 290.377);

	cout << "Конструктор без параметров\n";
	localityWithEmptyConstructor.output();
	system("pause");
	system("cls");
	

	cout << "Конструктор с двумя параметрами\n";
	localityWithTwoParamsConstructor.output();
	system("pause");
	system("cls");

	cout << "Конструктор со всеми параметрами\n";
	localityWithFullConstructor.output();
	system("pause");
	system("cls");

	// Ввод/вывод данных
	//---localityWithEmptyConstructor.input();
	//---localityWithEmptyConstructor.output();

	// Геттеры/сеттеры
	localityWithTwoParamsConstructor.setCitizenAmoutn(520300);
	localityWithTwoParamsConstructor.setArea(290.377);
	localityWithTwoParamsConstructor.setYearOfFoundation(1663);


	// Публичные методы
	cout << "Вызов методов класса\n";
	localityWithTwoParamsConstructor.printPopulationDensity();
	localityWithTwoParamsConstructor.printCurrentAge();
	system("pause");
	system("cls");

	Locality localityList[length];
	for (int i = 0; i < length; i++) {
		localityList[i].input();
		cout << "\n";
		localityList[i].output();
	}
	system("pause");
}