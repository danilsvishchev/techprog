#pragma once
#include <iostream>
using namespace std;

class Coordinates{
private:
	double latitude;
	double	longitude;

public:
	Coordinates() {
		this->latitude = 0;
		this->longitude = 0;
	}
	Coordinates(double latitude, double	longitude) {
		this->latitude = latitude;
		this->longitude = longitude;
	}

	~Coordinates(){
		this->latitude = 0;
		this->longitude = 0;
	}

	double getLatitude() {
		return latitude;
	}

	void setLatitude(double latitude) {
		this->latitude = latitude;
	}

	double getLongitude() {
		return longitude;
	}

	void setLongitude(double longitude) {
		this->longitude = longitude;
	}

	void input() {
		cout << "����������:\n";
		cout << "-������: ";
		cin >> this->latitude;
		cout << "-�������: ";
		cin >> this->longitude;
	}

	void output() {
		cout << "����������: " << this->latitude << " (������) " << this->longitude << " (�������)" << endl;
	}
};