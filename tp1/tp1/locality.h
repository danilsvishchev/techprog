#pragma once
#include <iostream>
#include <iomanip>
#include <cstring>
#include <ctime>
#include "coordinates.h"
using namespace std;

const int nameLength = 31;
const int countryLength = 31;

class Locality {
private:
	char name[nameLength];
	Coordinates coordinates;
	char country[nameLength];
	double area;

	int curruntAge() {
		time_t now = time(0);
		tm* localtm = localtime(&now);
		return localtm->tm_year + 1900 - this->yearOfFoundation;
	}

protected:
	int citizenAmoutn;
	double populationDensity() {
		return (double) this->citizenAmoutn / area;
	}

public:
	int yearOfFoundation;
	Locality() {
		strncpy(name, "", nameLength);
		this->citizenAmoutn = 0;
		this->coordinates = Coordinates();
		this->yearOfFoundation = 0;
		strncpy(country, "", countryLength);
		this->area = 0;
	}

	Locality(const char _name[], const char _country[]) {
		strncpy(name, _name, nameLength);
		strncpy(country, _country, countryLength);
		this->citizenAmoutn = 0;
		this->coordinates = Coordinates();
		this->yearOfFoundation = 0;
		this->area = 0;
	}

	Locality(const char _name[], int _citizenAmoutn, Coordinates _coordinates, int _yearOfFoundation, const char _country[], double _area) {
		strncpy(name, _name, nameLength);
		this->citizenAmoutn = _citizenAmoutn;
		this->coordinates = _coordinates;
		this->yearOfFoundation = _yearOfFoundation;
		strncpy(country, _country, countryLength);
		this->area = _area;
	}

	~Locality(){
		strncpy(name, "", nameLength);
		this->citizenAmoutn = 0;
		this->coordinates = Coordinates();
		this->yearOfFoundation = 0;
		strncpy(country, "", countryLength);
		this->area = 0;
	}

	char* getName() {
		return name;
	}

	void setName(const char _name[]) {
		strncpy(name, "", nameLength);
	}

	int getCitizenAmoutn() {
		return citizenAmoutn;
	}

	void setCitizenAmoutn(int _citizenAmoutn) {
		this->citizenAmoutn = _citizenAmoutn;
	}

	Coordinates getCoordinates() {
		return coordinates;
	}

	void setCoordinates(Coordinates _coordinates) {
		this->coordinates = _coordinates;
	}

	int getYearOfFoundation() {
		return yearOfFoundation;
	}

	void setYearOfFoundation(int _yearOfFoundation) {
		this->yearOfFoundation = _yearOfFoundation;
	}

	char* get�ountry() {
		return country;
	}

	void set�ountry(const char _country[]) {
		strncpy(country, "", countryLength);
	}

	double getArea() {
		return area;
	}

	void setArea(double _area) {
		this->area = _area;
	}

	void printPopulationDensity() {
		cout << "��������� ���������:" << populationDensity() << "(���./��^2)\n";
	}

	void printCurrentAge() {
		cout << "���������� ���:" << curruntAge() << "\n";
	}

	void input() {
		cout << "���� ������ - ������ '���������� �����'\n";
		cout << "�������� ������: ";
		cin >> this->name;
		cout << "���������� ������� ������: ";
		cin >> this->citizenAmoutn;
		coordinates.input();
		cout << "��� ��������� ������: ";
		cin >> this->yearOfFoundation;
		cout << "������: ";
		cin >> this->country;
		cout << "�������: ";
		cin >> this->area;
		cout << "\n";
	}

	void output() {
		cout << " ------------------------------------------------------------------------\n";
		cout << "| ����� ������ - ������ '���������� �����'                              |\n";
		cout << " ------------------------------------------------------------------------\n";
		cout << "|" << setw(38) << "�������� ������ | " << setw(32) << this->name << " |"<< endl;
		cout << " ------------------------------------------------------------------------\n";
		cout << "|" << setw(38) << "���������� ������� ������ | " << setw(32) << this->citizenAmoutn << " |" << endl;
		cout << " ------------------------------------------------------------------------\n";
		cout << "|" << setw(38) << "���������� | " << setw(16) << this->coordinates.getLatitude() << setw(16) << this->coordinates.getLongitude() << " |" << endl;
		//coordinates.output();
		cout << " ------------------------------------------------------------------------\n";
		cout << "|" << setw(38) << "��� ��������� ������ | " << setw(32) << this->yearOfFoundation << " |" << endl;
		cout << " ------------------------------------------------------------------------\n";
		cout << "|" << setw(38) << "������ | " << setw(32) << this->country << " |" << endl;
		cout << " ------------------------------------------------------------------------\n";
		cout << "|" << setw(38) << "������� | " << setw(32) << this->area << " |" << endl;
		cout << " ------------------------------------------------------------------------\n\n";
	}
};